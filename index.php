<?php
require_once "src\orm\utils\ClassLoader.php";
$loader = new orm\utils\ClassLoader('src');
$loader->register();

use orm\Query;
use orm\Model;
use model\Article;
use model\Categorie;
use orm\utils\ConnectionFactory;

$conf = parse_ini_file('conf/db.conf.ini');
ConnectionFactory::makeConnection($conf);

echo "mini ORM<br><br>";




echo "<br><br><b>4 /la classe Model : insert et delete </b><br>";

$ar = new Article(
    [
        'nom' => 'quentin',
        'descr' => 'ceci est la description de quentin',
        'tarif' => '8.00',
        'id_categ' => '8'
    ]
);

echo "(code commenté)<br><br><br>";
//$ar->insert();
//$ar->delete();


echo "<br><br><b>5-1 / Article::all()</b><br>";

$liste = Article::all() ;
foreach ($liste as $article){
    echo "$article->nom<br>" ;
}

echo "<br><br><b>5-2-a / find avec un entier, find(66) </b><br>";
$article = Article::find(66) ;
echo "l'article 66 est $article->nom<br>";
var_dump($article);

echo "<br><br><b>5-2-b / find(66, ['nom', 'tarif']) </b><br>";
$art66 = Article::find(66, ['nom', 'tarif']);
var_dump($art66);


echo "<br><br><b>5-2-c / find( ['tarif', '>=', 60 ], ['nom', 'tarif']) </b><br>";
$listeArticles = Article::find( ['tarif', '>=', 60 ], ['nom', 'tarif']);
var_dump($listeArticles);

echo "<br><br><b>5-2-d / find( [['tarif', '>=', 60 ], ['id_categ', '>=', '3']]) </b><br>";
$listeArticles = Article::find( [['tarif', '>=', 60 ], ['id_categ', '>=', '3']]);
var_dump($listeArticles);


echo "<br><br><b>5-2-e / first(1) puis first(['id', '=', '1'])</b><br>";

echo "<b>first(1) : </b><br>";

$article = Article::first(1) ;
echo "l'article 1 est $article->nom<br>";
var_dump($article);

echo "<br><br><b>first(['id', '=', '1']) : </b><br>";
$article2 = Article::first(['id', '=', '1']);
var_dump($article2);




echo "<br><br><br>";
echo "<br><br><b>6-1 / obtenir la catégorie d'un article, first(78) et belongs_to </b><br>";

$a = Article::first(78);
$cat= $a->belongs_to("\model\Categorie", "id_categ");
echo "la catégorie est : $cat->nom<br>";
var_dump($cat);



echo "<br><br> <b>6-2/ obtenir la liste d'articles d'une categ, first(8) et has_many</b><br>";

$c = Categorie::first(8);
$list = $c->has_many("\model\Article", "id_categ");
foreach ($list as $val){
    echo "$val->nom";
    echo "<br><br>";
}
var_dump($list);



echo "<br><br><b>Question 6 - 4 /</b><br><br>";

$c = Categorie::first(8);
$list = $c->articles() ;
var_dump($list);
echo "ne fonctionne pas";
//
//foreach ($list as $val){
//    print_r($val);
//    echo "<br><br>";
//}













































