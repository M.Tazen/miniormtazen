
# Installation :

Ce projet s'intalle via Composer





# Consignes

L'index présente la liste des fonctionnalités demandées de la
question 4 à la question 6. Les questions sont mises en gras
et les résultats sont montrés via un var_dump.
Ces questions sont basées sur le bon fonctionnement des classes
Query et Model, et ne sont donc pas testée dans l'index.





# Notes Supplémentaires

1/Les requetes qui renvoient des tableaux ne fonctionnent pas
toujours bien. On ne peut pas les lister avec un foreach qui
accèderait à l'attribut d'une valeur. D'où le choix de montrer
les résultat via des var_dump.

2/Il faut décommenter les lignes 32 et 33 de l'index afin de
tester l'insertion et la suppression.