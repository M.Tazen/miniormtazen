<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 21/10/2018
 * Time: 06:30
 */

namespace model;

use orm\Model;

class Article extends Model
{

    public static $table      = 'article';
    public static $primaryKey = 'id';

    public function categorie(){
        return $this->belongs_to("\model\Categorie", "id_categ");
    }

}