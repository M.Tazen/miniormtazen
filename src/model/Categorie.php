<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 12/12/2018
 * Time: 10:07
 */

namespace model;

use orm\Model;


class Categorie extends Model
{

    public static $table      = 'categorie';
    public static $primaryKey = 'id';


    public function articles(){
        return $this->has_many("\model\Article", "id_categ");
    }


}