<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 09/10/2018
 * Time: 10:17
 */

namespace orm;

use orm\utils\ConnectionFactory;

//require_once 'ConnectionFactory.php';

class Query
{
    private $sqltable;
    private $fields = '*';
    //private $where = null;
    private $where = [];
    private $args = [];
    private $sql;


    public static function table(string $table) {
        $query = new Query();
        $query->sqltable = $table;
        return $query;
    }

    //called through where()
    private function whereCOV ($colonne, $op, $valeur) {
        //$this->where = $colonne .$op ."?";
        array_push($this->where, $colonne .$op ."?");
        array_push($this->args, $valeur);
        return $this;
    }

    //called through where()
    private function whereTab ($criteria) {
        //$this->where = $colonne .$op ."?";
        if(is_array($criteria[0])){
            foreach ($criteria as $tab){
                array_push($this->where, $tab[0] .$tab{1} ."?");
                array_push($this->args, $tab[2]);
            }
        } else {
            array_push($this->where, $criteria[0] .$criteria{1} ."?");
            array_push($this->args, $criteria[2]);
        }

        return $this;
    }

    //handles both ways to call where
    public function __call ($method, $arguments){
        switch ($method){
            case 'where' :
                if(count($arguments) == 1) {
                    return call_user_func_array(array(
                        get_called_class(),'whereTab'), $arguments);
                } else  {
                    return call_user_func_array(array(
                        get_called_class(),'whereCOV'), $arguments);
                }
                break;
        }
    }


    public function get() {
        $this->sql =
            "select " .$this->fields
            ." from " .$this->sqltable;

        if (!empty($this->where)){
            $this->sql .= " where " .$this->where[0]; //.str_replace("?",$this->args[0], $this->where[0]);
            $this->add();
        }

        $pdo = ConnectionFactory::getConnection() ;
        $stmt = $pdo->prepare($this->sql);
        $stmt->execute($this->args);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        //return $this;
    }

    private function add() {
        $length = count($this->args);
        if ($length>0){
            for ($i=1; $i<$length; $i++)
                $this->sql .= " and " .$this->where[$i];
        }
    }

    public function select(array $fields=[]) {
        if(!empty($fields)){
            $this->fields = implode( ',', $fields);
        }
        return $this;
    }

    public function delete(){
        $this->sql =
            "delete"
            ." from " .$this->sqltable;
        if (!empty($this->where)){
            $this->sql .= " where "  .$this->where[0];
        }
        $this->add();

        return $this->connectDb();
        //return $this;
    }

    public function insert(array $tab) {
        $this->sql =
            'insert into ' .$this->sqltable ." (";

        $i=0;
        foreach ($tab as $key => $value) {
            if ($i == count($tab)-1){
                $this->sql .= $key .")";
            } else {
                $this->sql .= $key .",";
            }
            $i++;
        }

        $this->sql .= ' values (';
        $i=0;
        foreach ($tab as $key => $value) {
            if ($i == count($tab)-1){
                $this->sql .= "?)";
            } else {
                $this->sql .= "?,";
            }
            array_push($this->args, $value);
            $i++;
        }

        $pdo = ConnectionFactory::getConnection();
        $stmt = $pdo->prepare($this->sql);
        $stmt->execute($this->args);
        return (int) $pdo->lastInsertId($this->sqltable);
    }

    public function all() {
        $this->sql = ' SELECT * FROM ' .$this->sqltable;
        return $this->connectDb();
    }

    private function connectDb(){
        $pdo = ConnectionFactory::getConnection() ;
        $stmt = $pdo->prepare($this->sql);
        $stmt->execute($this->args);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }





















}