<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 21/10/2018
 * Time: 06:29
 */

namespace orm;

abstract class Model
{

    private $arrayModel = [];

    public function __construct ($tab=[]) {
        if(!is_null($tab)){
            $this->arrayModel = $tab;
        }
    }

    public function delete(){
        $queryA=Query::table(static::$table)
            ->where(static::$primaryKey, "=", $this->arrayModel['id'])
            ->delete();
    }

    public function insert() {
        $id=Query::table(static::$table)
            ->insert($this->arrayModel);
        $this->arrayModel[static::$primaryKey] = (int) $id;
        return $id;
    }

    public static function all(){
        $queryAll=Query::table(static::$table)
            ->all();
        $listeObjet=[];
        foreach ($queryAll as $ligne){
            array_push($listeObjet, new Static($ligne));
        }
//        var_dump($listeObjet);
        return $listeObjet;
    }

    //called through find()
    private static function findWithId($id){
        $queryFind=Query::table(static::$table)
                ->select()
                ->where(static::$primaryKey, "=", $id)
                ->get();

        return new Static($queryFind[0]);
    }

    //called through find()
    private static function findWithIdAndColumn($id, $nomColumn){
        foreach ($nomColumn as $key => $value) {
                $colonnes[$key] = $value;
        }

        $queryFind=Query::table(static::$table)
            ->select($colonnes)
            ->where(static::$primaryKey, "=", $id)
            ->get();
var_dump($queryFind);
        return new Static($queryFind[0]);
    }

    //called through find()
    private static function findWithCriteriaAndColumn($criteria, $nomColumn){
        foreach ($nomColumn as $key => $value) {
            $colonnes[$key] = $value;
        }

        $queryFind=Query::table(static::$table)
            ->select($colonnes)
            ->where($criteria)
            ->get();

        return new Static($queryFind);
    }

    //called through find()
    private static function findWithSeveralCriterias($criterias){
        $queryFind=Query::table(static::$table)
            ->select()
            ->where($criterias)
            ->get();

        return new Static($queryFind);
    }

    //called through first()
    private static function firstById($id){
        $res = self::findWithId($id);
        return $res;
    }

    //called through first()
    private static function firstByCriterias($criterias){
        $res = self::findWithSeveralCriterias($criterias);
        return $res;
    }

    /* ----------------------- MAGIC CALL ---------------------------------*/

    public static function __callStatic ($method, $arguments){
        switch ($method){
            case 'find' :
                if(count($arguments) == 1) {
                    if (is_int($arguments[0])){
                        return call_user_func_array(array(
                            get_called_class(),'findWithId'), $arguments);

                    } else {
                        return call_user_func_array(array(
                            get_called_class(),'findWithSeveralCriterias'), $arguments);
                    }
                }
                else if(count($arguments) == 2) {
                    if (is_int($arguments[0])){
                        return call_user_func_array(array(
                            get_called_class(),'findWithIdAndColonnes'), $arguments);

                    } else {
                        return call_user_func_array(array(
                            get_called_class(),'findWithCriteriaAndColumn'), $arguments);
                    }
                }
                break;
            case 'first' :
                if (is_int($arguments[0])){
                    return call_user_func_array(array(
                        get_called_class(),'firstById'), $arguments);

                } else {
                    return call_user_func_array(array(
                    get_called_class(),'firstByCriterias'), $arguments);
                }
                break;
        }
    }


    /* -------------------------- ASSOCIATIONS ---------------------------------*/

    public function belongs_to($modelName, $id){
        $query=Query::table($modelName::$table)
            ->select()
            ->where([$modelName::$primaryKey, '=', $this->arrayModel[$id]])
            ->get();

        return new $modelName($query[0]);
    }

    public function has_many($modelName, $id){

        $query=Query::table($modelName::$table)
            ->select()
            ->where([$id, '=', $this->arrayModel[static::$primaryKey]])
            ->get();

        return new $modelName($query);
    }



    /* ----------------------- GETTERS AND SETTERS ------------------------------*/

    public function __get($property) {
        if (array_key_exists($property, $this->arrayModel)) {
            return $this->arrayModel[$property];
        } else {
            throw new \Exception('Cet attribut n\'exite pas : ' .$property);
        }
    }

    public function __set($property, $value){
        $this->arrayModel[$property] = $value;
    }

}


















