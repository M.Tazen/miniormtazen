<?php
/**
 * Created by PhpStorm.
 * User: tazen
 * Date: 16/10/2018
 * Time: 06:02
 */

namespace orm\utils;

use PDO;

class ConnectionFactory
{
    private static $dsn;
    private static $user;
    private static $pass;

    private static $db = null;

    private static $option = [
        PDO::ATTR_PERSISTENT => true,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES=> false,
        PDO::ATTR_STRINGIFY_FETCHES => false
    ];

    public static function makeConnection(array $conf) {
        try {
            self::$dsn = $conf["driver"].":".'host='.$conf["host"].";".'dbname='.$conf['database'];
            self::$user = $conf["username"];
            self::$pass = $conf["password"];
            //var_dump(self::$dsn);
            self::$db = new \PDO(self::$dsn,  self::$user,  self::$pass, self::$option);
        } catch (PDOException $e) {
            throw new Exception("Connexion non réussie");
        }
        return self::$db;
    }

    public static function getConnection() {
        if(is_null(self::$db)) {
            $conf = parse_ini_file('conf/db.conf.ini');
            self::makeConnection($conf);
        }
        return self::$db;
    }

}























